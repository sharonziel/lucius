package com.aym.lucius.app.security;

import com.aym.lucius.Application;
import com.aym.lucius.backend.data.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.configurers.ExpressionUrlAuthorizationConfigurer;
import org.springframework.security.config.annotation.web.configurers.FormLoginConfigurer;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.security.core.session.SessionRegistryImpl;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.security.web.authentication.session.CompositeSessionAuthenticationStrategy;
import org.springframework.security.web.authentication.session.RegisterSessionAuthenticationStrategy;
import org.springframework.security.web.authentication.session.SessionAuthenticationStrategy;
import org.springframework.security.web.authentication.session.SessionFixationProtectionStrategy;

import java.util.LinkedList;
import java.util.List;

/**
 * Using example from https://examples.javacodegeeks.com/enterprise-java/vaadin/vaadin-spring-security-example/
 *
 * Also using: https://docs.spring.io/spring-security/site/docs/current/reference/html/jc.html
 *
 */
@EnableWebSecurity
//@ComponentScan
@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private final UserDetailsService userDetailsService;

    //private final PasswordEncoder passwordEncoder;

    private final RedirectAuthenticationSuccessHandler successHandler;

    @Autowired
    public SecurityConfig(UserDetailsService userDetailsService, /*PasswordEncoder passwordEncoder,*/
                          RedirectAuthenticationSuccessHandler successHandler) {
        this.userDetailsService = userDetailsService;
        //this.passwordEncoder = passwordEncoder;
        this.successHandler = successHandler;
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        super.configure(auth);
        //auth.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder);
        //auth.authenticationProvider(authenticationProvider());
    }

    @Bean
    public UserDetailsService userDetailsService() {
        InMemoryUserDetailsManager manager = new InMemoryUserDetailsManager();
        manager.createUser(User.withDefaultPasswordEncoder().username("user").password("password").roles("USER").build());
        return manager;
    }

    @Bean
    public DaoAuthenticationProvider authenticationProvider() {
        final DaoAuthenticationProvider authProvider = new DaoAuthenticationProvider();
        authProvider.setUserDetailsService(userDetailsService);
        //authProvider.setPasswordEncoder(passwordEncoder);
        return authProvider;
    }

    // This comes from vaadin-spring-security-example
    @Override
    protected void configure(final HttpSecurity http) throws Exception {
        http
            .csrf().disable()
            .authorizeRequests()
                .antMatchers("/VAADIN/**", "/HEARTBEAT/**", "/UIDL/**", "/login", "/login**", "/login/**").permitAll()
            .anyRequest().authenticated()
                .antMatchers("/app").access("hasAuthority('USE-APP-ROLE')").and()
            .formLogin().loginPage("/login?auth").permitAll().defaultSuccessUrl("/app", true).and()
            .sessionManagement().sessionAuthenticationStrategy(sessionControlAuthenticationStrategy());
    }

    @Bean
    public SessionAuthenticationStrategy sessionControlAuthenticationStrategy() {
        SessionFixationProtectionStrategy strategy = new SessionFixationProtectionStrategy();
        strategy.setMigrateSessionAttributes(false);

        RegisterSessionAuthenticationStrategy register = new RegisterSessionAuthenticationStrategy(sessionRegistry());
        List strategies = new LinkedList();
        strategies.add(strategy);
        strategies.add(register);

        CompositeSessionAuthenticationStrategy compositeStrategy = new CompositeSessionAuthenticationStrategy(strategies);
        return compositeStrategy;
    }

    @Bean
    public SessionRegistry sessionRegistry() {
        SessionRegistry sessionRegistry = new SessionRegistryImpl();
        return sessionRegistry;
    }

    // This part borrowed from v8-starter
//    @Override
//    protected void configure(HttpSecurity http) throws Exception {
//        // Not using Spring CSRF here to be able to use plain HTML for the login
//        // page
//        http.csrf().disable();
//
//        ExpressionUrlAuthorizationConfigurer<HttpSecurity>.ExpressionInterceptUrlRegistry reg = http
//                .authorizeRequests();
//
//        // Allow access to static resources ("/VAADIN/**")
//        reg = reg.antMatchers("/VAADIN/**").permitAll();
//        reg = reg.antMatchers("/login/**").permitAll();
//        // Require authentication for all URLS ("/**")
//        reg = reg.antMatchers("/**").hasAnyAuthority(Role.getAllRoles());
//        HttpSecurity sec = reg.and();
//
//        // Allow access to login page without login
//        FormLoginConfigurer<HttpSecurity> login = sec.formLogin().permitAll();
//        login = login.loginPage(Application.LOGIN_URL).loginProcessingUrl(Application.LOGIN_PROCESSING_URL)
//                .failureUrl(Application.LOGIN_FAILURE_URL).successHandler(successHandler);
//        login.and().logout().logoutSuccessUrl(Application.LOGOUT_URL);
//    }
}
