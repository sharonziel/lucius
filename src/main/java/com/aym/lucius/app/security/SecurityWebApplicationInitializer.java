package com.aym.lucius.app.security;

import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;
import org.springframework.security.web.session.HttpSessionEventPublisher;

import javax.servlet.ServletContext;
import javax.servlet.annotation.WebListener;

/**
 * This class will initiate the spring security frameworks
 *
 * https://examples.javacodegeeks.com/enterprise-java/vaadin/vaadin-spring-security-example/
 */
@WebListener
public class SecurityWebApplicationInitializer extends AbstractSecurityWebApplicationInitializer {

    public SecurityWebApplicationInitializer() {
        super(SecurityConfig.class);
    }

    @Override
    protected void beforeSpringSecurityFilterChain(ServletContext servletContext) {
        super.beforeSpringSecurityFilterChain(servletContext);
        servletContext.addListener(new HttpSessionEventPublisher());
    }
}
