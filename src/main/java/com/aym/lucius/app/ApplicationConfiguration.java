package com.aym.lucius.app;

import com.vaadin.spring.access.SecuredViewAccessControl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ApplicationConfiguration {

    // Might be useful someday
    /*@Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }*/

    // Vaadin Addon
    @Bean
    SecuredViewAccessControl securedViewAccessControl()
    {
        return new SecuredViewAccessControl();
    }
}
