package com.aym.lucius.app;

import com.vaadin.server.ServiceInitEvent;
import com.vaadin.server.VaadinService;
import com.vaadin.server.VaadinServiceInitListener;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

/**
 * Configures the VaadinService instance that serves the app through a servlet.
 *
 * Bootstrap listeners added here
 * 'Borrowed' from V8-Starter
 *
 */
@Component
public class ApplicationInitListener implements VaadinServiceInitListener {

    @Override
    public void serviceInit(ServiceInitEvent serviceInitEvent) {
        VaadinService service = serviceInitEvent.getSource();

        // Leaving this here as a reminder .. in case we need to add something later.
        //((VaadinService) service).addSessionInitListener(event -> event.getSession().addBootstrapListener(new BlahBootstrapListener()));
    }
}
