package com.aym.lucius.backend.service;

import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

@Service
public class AuthUserLookUpService {

    UserDetails findUser(String username) {
        UserDetails found = null;

        switch (username) {
            case "admin" : found = User.withUsername(username).build();
            case "user" : found = User.withUsername(username).build();
        }
        return found;
    }
}
