package com.aym.lucius.backend.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class AuthUserDetailsService implements UserDetailsService {

    @Autowired
    private AuthUserLookUpService authUserLookUpService;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        return authUserLookUpService.findUser(username);
    }
}
