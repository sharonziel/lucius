package com.aym.lucius.backend.service;

import com.aym.lucius.backend.RestClient;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

@Service
public class LoginService {

    /**
     * This currently does not work.
     */
    private void doStatefulLogin(String username, String password, String urlBase) {

        RestClient client = new RestClient();

        String strCompanyId = "303";
        //String strUsername = this.userName.getValue();
        //String strPassword = this.password.getValue();
        String strImpersonateUser = "false";
        String url = urlBase + "/j_spring_security_check";

        String response = client.login(username, password);

            /*RestTemplate restTemplate = new RestTemplate();
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
            headers.setAccessControlRequestMethod(HttpMethod.POST);

            // Add our form data
            MultiValueMap<String, String> map= new LinkedMultiValueMap<String, String>();
            map.add("j_username", strUsername);
            map.add("j_password", strPassword);
            map.add("companyId", strCompanyId);
            map.add("pocHack", "true");

            // Make POST request
            HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<MultiValueMap<String, String>>(map, headers);

            ResponseEntity<Object> response = client.template().exchange(url, HttpMethod.POST, request , Object.class );
            */

        System.out.println(response.toString());
    }

    /**
     * Poked hole in Spring security, so now lets see if we can get in
     */
    public void getEmployeeList(String urlBase) {

        String url = urlBase + "/action/getEmployeeSearchList?limit=75&start=0&page=1";

        // Make a request ..
        //ExtDirectStoreReadResult edsrResult = client.template().getForObject(url, ExtDirectStoreReadResult.class);
        //url, HttpMethod.POST, request , ExtDirectStoreReadResult.class );

        // This does NOT work
        //RestTemplate template = new RestTemplate();
        //ExtDirectStoreReadResult edsrResult = template.getForObject(url, ExtDirectStoreReadResult.class);

        RestTemplate temp = new RestTemplate();
        String result = temp.getForObject(url, String.class);

        // Check result
        System.out.println("GetEmployeeSearch:" + result.toString());
    }

    /**
     *
     */
    public void doStatefulLogout() {

    }

    /**
     * This does not work yet - 404 when doing a POST, however GET works fine.
     * WTF?
     */
    public String doPostLogin(String username, String password, String strUrlBasePath) {

        String strCompanyId = "303";
        //String strUsername = this.userName.getValue();
        //String strPassword = this.password.getValue();
        String strImpersonateUser = "false";
        String url = strUrlBasePath + "login/doPostLogin";

        // Build HttpHeader
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        headers.setAccessControlRequestMethod(HttpMethod.POST);

        // Add our form data
        MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
        map.add("j_username", username);
        map.add("j_password", password);
        map.add("companyId", strCompanyId);
        map.add("pocHack", "true");

        // Make POST request
        HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<MultiValueMap<String, String>>(map, headers);
        ResponseEntity<Object> response = restTemplate.exchange(url, HttpMethod.POST, request, Object.class);

        String xResText = response.toString();
        String xResStatus = response.getStatusCode().toString();

        return "Status:" + xResStatus + "\n ResponseEntity:" + xResText;
    }

    /**
     * Makes RESTful call to LoginAction Controller to authenticate
     */
    public String doLogin(String username, String password, String urlBase) {

        String strCompanyId = "303";
        String strImpersonateUser = "false";
        String url = urlBase + "/j_spring_security_check";

        // Build HttpHeader
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        headers.setAccessControlRequestMethod(HttpMethod.POST);

        // Add our form data
        MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
        map.add("j_username", username);
        map.add("j_password", password);
        map.add("companyId", strCompanyId);
        map.add("pocHack", "true");

        // Make POST request
        HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<MultiValueMap<String, String>>(map, headers);
        ResponseEntity<Object> response = restTemplate.exchange(url, HttpMethod.POST, request, Object.class);

        String xResText = response.toString();
        String xResStatus = response.getStatusCode().toString();

        // Response Body is null, but we do get a sessionId FWIW
        return "Status:" + xResStatus + "; ResponseEntity:" + xResText;
    }


    /**
     * if we login, we should be able to logout.
     */
    public String doLogout(String urlBase) {

        String url = urlBase + "/action/logout";

        // Simple RESTful GET call to LoginAction controller
        RestTemplate temp = new RestTemplate();
        ResponseEntity<String> response = temp.exchange(url, HttpMethod.GET, null, String.class);

        return response.toString();
    }

    /**
     * This populates the combobox with list of companies linked to user
     * TODO: companyid is a hidden field
     *
     * @param username
     */
    public List<String> buildCompanyList(String username, String strUrlBasePath) {

        // At somepoint need to enable dynamic username
        System.out.println("In buildCompanyList()");
        System.out.println("Username=" + username);

        List<String> compList = new ArrayList<>();
        //String url = "http://localhost:8053/otcommon_war_exploded/action/login/getCompanies?username="+username;
        String url = strUrlBasePath + "/action/login/getCompanies?username=" + username;

        // RESTful call to LoginAction controller
        RestTemplate temp = new RestTemplate();
        String result = temp.getForObject(url, String.class);

        // Get ComboBox to work first
        try {
            JSONObject obj = new JSONObject(result);
            JSONArray comp = obj.getJSONArray("companies");

            // Add to list
            for (int i = 0; i < comp.length(); i++) {

                System.out.println(comp.get(i));

                // Lets parse out our company name
                // TODO: figure out why double bracket happens AND review regEx
                String strComp = comp.get(i).toString();

                // Outer brackets
                strComp = strComp.replace("[", "");
                strComp = strComp.replace("]", "");

                // everything left of and including comma
                int pos = strComp.indexOf(",");
                strComp = strComp.substring(pos++);

                // Remove Double quotes
                strComp = strComp.substring(2, strComp.length() - 1);

                System.out.println(strComp);

                compList.add(strComp);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return compList;
    }
}
