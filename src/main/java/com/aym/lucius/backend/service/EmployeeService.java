package com.aym.lucius.backend.service;

import com.aym.lucius.backend.entity.Employee;
import com.fasterxml.jackson.databind.util.JSONPObject;
import com.vaadin.server.FileResource;
import com.vaadin.server.VaadinService;
import javafx.util.converter.LocalDateTimeStringConverter;
import org.apache.commons.io.IOUtils;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.*;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

@Service
public class EmployeeService {

    /**
     *
     * @return
     */
    public List<Employee> getEmployeeSearchList() {

        ArrayList list = new ArrayList();
        list.add(new Employee("Bugs", "Bunny"));
        list.add(new Employee("Daffy", "Duck"));
        list.add(new Employee("Elmer", "Fudd"));

        return list;
    }

    /**
     *
     * @return
     */
    public List getEmployeeSearch()
    {

        ArrayList employeeList = new ArrayList();

        String fileName = "/getEmployeeSearchList.txt";
        String abPath = VaadinService.getCurrent().getBaseDirectory().getAbsolutePath();
        String sourceFile = abPath + fileName;

        // @TODO: Figure out how to read this from classpath
        // C:\projects\repos\..\src\main\webapp\getEmployeeSearchList.txt
        FileResource resource = new FileResource(new File(sourceFile));
        InputStream input = resource.getStream().getStream();

        String employeeJsonData;

        try {
            // Read our file into a string
            employeeJsonData = IOUtils.toString(input, StandardCharsets.UTF_8);

            // Sample Json file is an array
            JSONParser jsonParser = new JSONParser();

            // Parent node
            JSONArray array = (JSONArray)jsonParser.parse(employeeJsonData);

            //Iterator iter = array.iterator();
            //while (iter.hasNext()) {
            for (int i=0; i<array.size(); i++) {

                // Result Node
                //JSONObject jsonObject = (JSONObject) iter.next();
                JSONObject jsonObject = (JSONObject) array.get(i);
                String test1 = jsonObject.get("result").toString();
                JSONObject resultsJson = (JSONObject)jsonObject.get("result");

                // Records Node
                JSONArray records = (JSONArray) jsonParser.parse(resultsJson.get("records").toString());
                System.out.println("Size=" + employeeList.size());

                try {

                    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/dd/yyyy");

                    Iterator recordIter = records.iterator();
                    while (recordIter.hasNext()) {
                        JSONObject emplObj = (JSONObject) recordIter.next();
                        Employee employee = new Employee();
                        employee.setCode(emplObj.get("code").toString());
                        employee.setSsn(emplObj.get("ssn").toString());
                        employee.setSsnMasked(emplObj.get("ssnMasked").toString());
                        employee.setFirstNameMI(emplObj.get("firstNameMI").toString());
                        employee.setLastName(emplObj.get("lastName").toString());
                        employee.setDisplayValue(emplObj.get("displayValue").toString());
                        employee.setCredentials(emplObj.get("credentials").toString());
                        if (emplObj.get("dateOfBirth") != null)
                            employee.setDateOfBirth(LocalDate.parse(emplObj.get("dateOfBirth").toString(), formatter));
                        employee.setGender(emplObj.get("gender").toString());
                        employee.setDateEmployed(LocalDate.parse(emplObj.get("dateEmployed").toString(), formatter));
                        if (emplObj.get("dateTerminated") != null)
                            employee.setDateTerminated(LocalDate.parse(emplObj.get("dateTerminated").toString(), formatter));
                        if (emplObj.get("race.id") != null) {
                            employee.setRaceId(emplObj.get("race.id").toString());
                            employee.setRaceDescription(emplObj.get("race.description").toString());
                        }
                        if (emplObj.get("busEmpStatus") != null) {
                            employee.setBusEmpStatus(emplObj.get("busEmpStatus").toString());
                            employee.setBusEmpStatusDesc(emplObj.get("busEmpStatus.description").toString());
                        }

                        employeeList.add(employee);
                    }

                } catch (DateTimeParseException e) {
                    e.printStackTrace();
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return employeeList;
    }

}
