package com.aym.lucius.backend.entity;

/**
 * Entity class? Or just a PoJo
 */
public class Demographic {

    private String address1;
    private String address2;
    private String city;
    private String state;
    private String zip;
    private String checkAddress1;
    private String checkAddress2;
    private String checkCity;
    private String checkState;
    private String checkZip;
    private Integer allowableBillableHours;
    private Integer jobTitle;
    private String jobDesc;
    private Integer costCenter;
    private String costCenterDesc;
    private String payPeriod;
    private String payClass;
    private String homePhone;
    private String workPhone;
    private String mobilePhone;
    private String workEmailAddress;
    private String homeEmailAddress;
    private String driversLicense;
    private String emergencyContactName;
    private String emergencyContactNumber;
    private String preferredLanguage;
    private Integer daysUnemployed;
    private String note;

    public Demographic() {
    }
}
