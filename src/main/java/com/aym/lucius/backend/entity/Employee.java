package com.aym.lucius.backend.entity;

import org.apache.tomcat.jni.Local;

import java.time.LocalDate;

/**
 * Entity
 */
public class Employee extends AbstractEntity {

    private String code;
    private String ssn;
    private String ssnMasked;
    private String displayValue;
    private String firstNameMI;
    private String lastName;
    private String gender;
    private LocalDate dateOfBirth;
    private LocalDate dateTerminated;
    private LocalDate dateEmployed;
    private String raceId;
    private String raceDescription;
    private String busEmpStatus;
    private String busEmpStatusDesc;
    private Demographic employeeDemographic;
    private String credentials;
//    private boolean ein;
//    private boolean inactive;
//    private boolean hasPicture;
//    private boolean eligibleForRehire;
//    private Integer reportsTo;
//    private String reportsToName;

    public Employee() {
    }

    /**
     *
     * @param firstNameMI
     * @param lastName
     */
    public Employee(String firstNameMI, String lastName) {
        this.firstNameMI = firstNameMI;
        this.lastName = lastName;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getSsnMasked() {
        return ssnMasked;
    }

    public void setSsnMasked(String ssnMasked) {
        this.ssnMasked = ssnMasked;
    }

    public String getSsn() {
        return ssn;
    }
    public void setSsn(String ssn) {
        this.ssn = ssn;
    }

    public String getDisplayValue() {
        return displayValue;
    }

    public void setDisplayValue(String displayValue) {
        this.displayValue = displayValue;
    }

    public String getFirstNameMI() {
        return firstNameMI;
    }

    public void setFirstNameMI(String firstNameMI) {
        this.firstNameMI = firstNameMI;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public LocalDate getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(LocalDate dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public LocalDate getDateTerminated() {
        return dateTerminated;
    }

    public void setDateTerminated(LocalDate dateTerminated) {
        this.dateTerminated = dateTerminated;
    }

    public LocalDate getDateEmployed() {
        return dateEmployed;
    }

    public void setDateEmployed(LocalDate dateEmployed) {
        this.dateEmployed = dateEmployed;
    }

    public String getRaceId() {
        return raceId;
    }

    public void setRaceId(String raceId) {
        this.raceId = raceId;
    }

    public String getRaceDescription() {
        return raceDescription;
    }

    public void setRaceDescription(String raceDescription) {
        this.raceDescription = raceDescription;
    }

    public String getBusEmpStatus() {
        return busEmpStatus;
    }

    public void setBusEmpStatus(String busEmpStatus) {
        this.busEmpStatus = busEmpStatus;
    }

    public String getBusEmpStatusDesc() {
        return busEmpStatusDesc;
    }

    public void setBusEmpStatusDesc(String busEmpStatusDesc) {
        this.busEmpStatusDesc = busEmpStatusDesc;
    }

    public Demographic getEmployeeDemographic() {
        return employeeDemographic;
    }

    public void setEmployeeDemographic(Demographic employeeDemographic) {
        this.employeeDemographic = employeeDemographic;
    }

    public String getCredentials() {
        return credentials;
    }

    public void setCredentials(String credentials) {
        this.credentials = credentials;
    }

//    public boolean isEin() {
//        return ein;
//    }
//
//    public void setEin(boolean ein) {
//        this.ein = ein;
//    }
//
//    public boolean isInactive() {
//        return inactive;
//    }
//
//    public void setInactive(boolean inactive) {
//        this.inactive = inactive;
//    }
//
//    public boolean isHasPicture() {
//        return hasPicture;
//    }
//
//    public void setHasPicture(boolean hasPicture) {
//        this.hasPicture = hasPicture;
//    }
//
//    public boolean isEligibleForRehire() {
//        return eligibleForRehire;
//    }
//
//    public void setEligibleForRehire(boolean eligibleForRehire) {
//        this.eligibleForRehire = eligibleForRehire;
//    }
//
//    public Integer getReportsTo() {
//        return reportsTo;
//    }
//
//    public void setReportsTo(Integer reportsTo) {
//        this.reportsTo = reportsTo;
//    }
//
//    public String getReportsToName() {
//        return reportsToName;
//    }
//
//    public void setReportsToName(String reportsToName) {
//        this.reportsToName = reportsToName;
//    }


}
