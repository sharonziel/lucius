package com.aym.lucius.ui;

import com.aym.lucius.app.HasLogger;
import com.aym.lucius.app.security.SecurityUtils;
import com.aym.lucius.backend.service.LoginService;
import com.aym.lucius.ui.navigation.NavigationManager;
import com.aym.lucius.ui.views.login.LoginView;
import com.vaadin.annotations.Theme;
import com.vaadin.annotations.Title;
import com.vaadin.annotations.Viewport;
import com.vaadin.server.DefaultErrorHandler;
import com.vaadin.server.VaadinRequest;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.spring.navigator.SpringViewProvider;
import com.vaadin.ui.UI;
import org.springframework.beans.factory.annotation.Autowired;
import sun.applet.Main;
import javax.swing.*;

/**
 * Where the magic happens.
 */
@Theme("valo")
@SpringUI(path="/app")
//@Viewport() // Needed yet?
@Title("Work dammit")
public class AppUI extends UI implements HasLogger {

    private final SpringViewProvider viewProvider;
    private final NavigationManager navigationManager;
    private final MainView mainView;
    private boolean isLoggedIn = false;

    @Autowired
    public AppUI(SpringViewProvider viewProvider, NavigationManager navigationManager, MainView mainView) {
        this.viewProvider = viewProvider;
        this.navigationManager = navigationManager;
        this.mainView = mainView;
    }

    @Override
    protected void init(VaadinRequest vaadinRequest) {
        setErrorHandler(event -> {
            Throwable t = DefaultErrorHandler.findRelevantThrowable(event.getThrowable());
            getLogger().error("Error during request", t);
        });

        // To add later when security enabled
        //viewProvider.setAccessDeniedViewClass(AccessDeniedView.class);

        //if (!isLoggedIn) {
        //    setContent(new LoginView());
        //    navigationManager.navigateTo(LoginView.class);

        //} else {
            setContent(mainView);
            navigationManager.navigateToDefaultView();
        //}
    }
}
