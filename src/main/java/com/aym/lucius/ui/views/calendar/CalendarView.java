package com.aym.lucius.ui.views.calendar;

import com.aym.lucius.ui.navigation.NavigationManager;
import com.aym.lucius.ui.views.calendar.meeting.MeetingCalendar;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.navigator.View;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.vaadin.addon.calendar.Calendar;

import javax.annotation.PostConstruct;
import java.time.Month;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.TextStyle;
import java.time.temporal.TemporalQueries;
import java.util.Locale;

/**
 * Need to figure out how to add items https://vaadin.com/forum/thread/16204256
 *
 * SZ: Commented out all uses of setFullSize in CalendarView and MeetingCalendar.
 *  It was causing the calendar to shrink and display at the bottom.
 *
 */
@SpringView
public class CalendarView extends CalendarViewDesign implements View {

    private final NavigationManager navigationManager;

    @Autowired
    public CalendarView(NavigationManager navigationManager) {
        this.navigationManager = navigationManager;
    }

    @PostConstruct
    public void init() {

        setResponsive(true);

        // SZ: Commenting this out everywhere, it made the calendar shrink
        // TODO: Maybe at some point figure out why, but whatever it works better.
        //this.setSizeFull();

        MeetingCalendar meetings =  new MeetingCalendar();
        //meetings.setSizeFull();

        ComboBox<Month> months = new ComboBox<>();
        months.setItems(Month.values());
        months.setItemCaptionGenerator(month -> month.getDisplayName(TextStyle.FULL, meetings.getCalendar().getLocale()));
        months.setEmptySelectionAllowed(false);
        months.addValueChangeListener(me -> meetings.switchToMonth(me.getValue()));

        Button today = new Button("today", (Button.ClickEvent clickEvent) -> meetings.getCalendar().withDay(ZonedDateTime.now()));
        Button week = new Button("week", (Button.ClickEvent clickEvent) -> meetings.getCalendar().withWeek(ZonedDateTime.now()));

        HorizontalLayout nav = new HorizontalLayout(months, today, week);
        //HorizontalLayout nav = new HorizontalLayout(calActionComboBox, zoneBox, fixedSize, fullSize, months, today, week);
        //nav.setWidth("100%");

        // Test out adding Calendar Items here?
        //meetings.set

        //Show in the middle of the screen
        final VerticalLayout layout = new VerticalLayout();
        layout.setStyleName("demoContentLayout");
        //layout.setSizeFull();
        layout.setMargin(true);
        layout.setSpacing(true);
        layout.addComponent(nav);
        layout.addComponentsAndExpand(meetings);
        addComponent(layout);  // Because setContent isnt allowed on the View.
    }

    /**
     * TODO: document once you figure out what this does.
     */
    private static class CalStyle {

        @FunctionalInterface
        interface CalAction {
            void update();
        }

        private String caption;
        private CalAction action;

        CalStyle(String caption, CalAction action) {
            this.caption = caption;
            this.action = action;
        }

        private void act() {
            action.update();
        }

        @Override
        public String toString() {
            return caption;
        }
    }

    /**
     * Place to hold extra code that I'm not using right now.
     * @param meetings
     */
    private void exampleCode(MeetingCalendar meetings) {

        // From example, don't need right now
        ComboBox<Locale> localeBox = new ComboBox<>();
        localeBox.setItems(Locale.getAvailableLocales());
        localeBox.setEmptySelectionAllowed(false);
        localeBox.setValue(UI.getCurrent().getLocale());
        localeBox.addValueChangeListener(e -> meetings.getCalendar().setLocale(e.getValue()));

        ComboBox<String> zoneBox = new ComboBox<>();
        zoneBox.setItems(ZoneId.getAvailableZoneIds());
        zoneBox.setEmptySelectionAllowed(false);
        zoneBox.setValue(meetings.getCalendar().getZoneId().getId());
        zoneBox.addValueChangeListener(e -> meetings.getCalendar().setZoneId(ZoneId.of(e.getValue())));

        ComboBox<CalStyle> calActionComboBox = new ComboBox<>();
        calActionComboBox.setItems(
                new CalStyle("Full Week (col 1-7)", () -> meetings.getCalendar().withVisibleDays(1, 7)),
                new CalStyle("Work Week (col 1-5)", () -> meetings.getCalendar().withVisibleDays(1, 5)),
                new CalStyle("Col 2 - 5", () -> meetings.getCalendar().withVisibleDays(2, 5)),
                new CalStyle("Col 6 - 7", () -> meetings.getCalendar().withVisibleDays(6, 7))
        );
        calActionComboBox.setEmptySelectionAllowed(false);
        calActionComboBox.addValueChangeListener(e -> e.getValue().act());

        Button fixedSize = new Button("fixed size", (Button.ClickEvent clickEvent) -> meetings.panel.setHeightUndefined());
        fixedSize.setIcon(VaadinIcons.LINK);

        Button fullSize = new Button("full size", (Button.ClickEvent clickEvent) -> meetings.panel.setHeight(100, Unit.PERCENTAGE));
        fullSize.setIcon(VaadinIcons.UNLINK);

        //HorizontalLayout nav = new HorizontalLayout(localeBox, zoneBox, fixedSize, fullSize, months, today, week);
    }
}
