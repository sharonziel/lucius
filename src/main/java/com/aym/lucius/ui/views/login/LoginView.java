package com.aym.lucius.ui.views.login;

import com.aym.lucius.backend.service.LoginService;
import com.vaadin.navigator.View;
import com.vaadin.spring.annotation.SpringView;
import org.springframework.beans.factory.annotation.Autowired;
import javax.annotation.PostConstruct;

@SpringView
public class LoginView extends LoginViewDesign implements View {


    @Autowired LoginService loginService;

    private LoginForm loginForm = new LoginForm(this);


    /**
     *
     */
    @PostConstruct
    public void init() {

        setResponsive(true);


    }



}
