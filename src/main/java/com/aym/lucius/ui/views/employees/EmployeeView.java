package com.aym.lucius.ui.views.employees;

import com.aym.lucius.backend.entity.Employee;
import com.aym.lucius.backend.service.EmployeeService;
import com.aym.lucius.ui.navigation.NavigationManager;
import com.vaadin.data.Binder;
import com.vaadin.navigator.View;
import com.vaadin.server.VaadinService;
import com.vaadin.server.VaadinServlet;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.Grid;
import com.vaadin.ui.UI;
import com.vaadin.ui.components.grid.ItemClickListener;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import java.io.File;
import java.util.List;

/**
 *
 */
@SpringView
public class EmployeeView extends EmployeeViewDesign implements View {

    private final NavigationManager navigationManager;

    // Form Layout
    //private EmployeeForm employeeForm = new EmployeeForm(this);

    private EmployeeForm employeeWindow = new EmployeeForm(this);

    private Grid<Employee> employeeGrid = new Grid<>();
    private int rowSize = 0;

    @Autowired
    protected EmployeeService employeeService;

    @Autowired
    public EmployeeView(NavigationManager navigationManager) {
        this.navigationManager = navigationManager;
    }

    /**
     *
     */
    @PostConstruct
    public void init() {

        setResponsive(true);

        addColumns();
        loadGridData();
        formatGrid();

        // Form Layout
        //toggleGridOn(true);

        // Selects the employee chosen from the grid for edit and passes to form for binding.
        // Form Layout
        /*employeeGrid.asSingleSelect().addValueChangeListener(event -> {
            if (event.getValue() == null) {
                employeeForm.setEmployee(new Employee());
                toggleGridOn(true);
            } else {
                employeeForm.setEmployee(event.getValue());
                toggleGridOn(false);
            }
        }); */

        // Form Layout
        //addComponents(employeeGrid, employeeForm);

        // Window
        employeeGrid.addItemClickListener(new ItemClickListener<Employee>() {
            @Override
            public void itemClick(Grid.ItemClick<Employee> itemClick) {
                if (itemClick.getMouseEventDetails().isDoubleClick()) {
                    employeeWindow.setEmployee(itemClick.getItem());
                    openFormWindow();
                }
            }
        });

        // Window
        addComponents(employeeGrid);

    }

    /**
     * Positioning didnt work so well, too strict
     *
     * setSizeUndefined is better, still not 100% sure how it exactly works
     * https://vaadin.com/forum/thread/39331
     *
     */
    protected void openFormWindow() {
        //employeeWindow.setHeight("70%");
        //employeeWindow.setWidth("75%");
        //employeeWindow.setPositionX(200);
        //employeeWindow.setPositionY(50);
        employeeWindow.getContent().setSizeUndefined();
        employeeWindow.center();
        UI.getCurrent().addWindow(employeeWindow);
    }

    /**
     * Form Layout
     * @param on
     */
    protected void toggleGridOn(boolean on) {
        //employeeGrid.setVisible(on);
        //employeeForm.setVisible(!on);
    }

    /**
     *
     */
    private void addColumns() {
        employeeGrid.addColumn(Employee::getCode).setCaption("Code");
        employeeGrid.addColumn(Employee::getSsnMasked).setCaption("SSN");
        employeeGrid.addColumn(Employee::getLastName).setCaption("LName");
        employeeGrid.addColumn(Employee::getCredentials).setCaption("Credentials");
        employeeGrid.addColumn(Employee::getDateOfBirth).setCaption("Date of Birth");
        employeeGrid.addColumn(Employee::getGender).setCaption("Gender");
        employeeGrid.addColumn(Employee::getDateEmployed).setCaption("Date Employed");
        employeeGrid.addColumn(Employee::getDateTerminated).setCaption("Date Terminated");
        employeeGrid.addColumn(Employee::getRaceDescription).setCaption("Race");
        employeeGrid.addColumn(Employee::getBusEmpStatusDesc).setCaption("Status");

    }

    /**
     *
     */
    private void formatGrid() {

        employeeGrid.setCaption("Testing Grid Caption");
        //employeeGrid.setHeight("100vh"); // vh not supported in V8, should match (%|px|em|rem|ex|in|cm|mm|pt|pc)
        //employeeGrid.setHeightByRows(rowSize-1);
        employeeGrid.setWidth("100%");

        //employeeGrid.setSizeFull();
    }

    /**
     * HACK to get the number of rows in the grid since V8 Grid SUX
     */
    private void loadGridData() {
        List employeeList = employeeService.getEmployeeSearch();
        rowSize = employeeList.size();
        employeeGrid.setItems(employeeList);
    }
}

