package com.aym.lucius.ui.views.login;

import com.vaadin.annotations.DesignRoot;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.declarative.Design;

@DesignRoot
public class LoginViewDesign extends VerticalLayout {

    public LoginViewDesign()  {
        Design.read(this);
    }
}

