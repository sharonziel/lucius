package com.aym.lucius.ui.views.login;

import com.aym.lucius.backend.entity.Employee;
import com.aym.lucius.backend.service.EmployeeService;
import com.aym.lucius.backend.service.LoginService;
import com.aym.lucius.ui.views.employees.EmployeeView;
import com.vaadin.ui.*;
import org.springframework.beans.factory.annotation.Autowired;

public class LoginForm extends VerticalLayout {

    // Default values
    String port = "38080";       // 8053 or 38080
    String path = ""; // 38080="" 8053="otcommon_war_exploded"
    String strUrlBasePath = "http://localhost:" + port + "/" + path;
    String pwdHack = "FSU_2013NaT10n@l-ChaMpz:z@kzMakesGo0dBurgers:t@c0zRgUd2l01ol";
    String usnHack = "ryan.mccalla";

    // Input Fields
    TextField userName = new TextField();
    TextField password = new TextField();
    ComboBox<String> companies = new ComboBox<>();

    // Action Buttons
    Button addUsn = new Button("Add Usn");
    Button addPwd = new Button("Add Spoof Pwd");
    Button btnGetEmployees = new Button("Get Employees");
    Button btnSubmit = new Button("Login");
    Button btnLogout = new Button("Logout");

    // Associated objects
    @Autowired private LoginService loginService;
    private LoginView view;

    /**
     *
     * @param view
     */
    public LoginForm(LoginView view) {
        // Window specific
        //super("Hope this works");

        this.view = view;

        userName.setCaption("Username");
        password.setCaption("Password");
        companies.setCaption("Company");

        userName.addBlurListener(e -> loginService.buildCompanyList(userName.getValue(), strUrlBasePath));

        // Enable the buttons
        // Too lazy to add usn
        addUsn.addClickListener(e -> {
            userName.setValue(usnHack);
        });
        addPwd.addClickListener(e -> {
            password.setValue(pwdHack);
        });

        // Enable Login button
        btnGetEmployees.addClickListener(e -> {
            loginService.getEmployeeList(strUrlBasePath);
        });

        // Enable Login button
        btnSubmit.addClickListener(e -> {
            //doLogin();
            //doStatefulLogin();
            loginService.doPostLogin(usnHack, pwdHack, strUrlBasePath);
        });

        // Enable Logout button
        btnLogout.addClickListener(e -> {
            loginService.doStatefulLogout();
            //logout();
        });
        // Position buttons and fields together
        HorizontalLayout hUsn = new HorizontalLayout();
        hUsn.addComponents(userName, addUsn);
        //hUsn.add(userName);

        HorizontalLayout hPwd = new HorizontalLayout();
        hPwd.addComponents(password, addPwd);

        // Finally lets add our login items to a 'login screen'
        this.addComponent(hUsn);
        this.addComponent(hPwd);
        this.addComponent(companies);
        this.addComponent(btnSubmit);
        this.addComponent(btnGetEmployees);
        this.addComponent(btnLogout);
        //add(loginForm);
    }
}
