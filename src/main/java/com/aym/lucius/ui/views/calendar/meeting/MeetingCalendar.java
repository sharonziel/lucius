package com.aym.lucius.ui.views.calendar.meeting;

import com.vaadin.shared.ui.ContentMode;
import com.vaadin.ui.*;
import org.vaadin.addon.calendar.Calendar;
import org.vaadin.addon.calendar.handler.BasicDateClickHandler;
import org.vaadin.addon.calendar.item.BasicItemProvider;
import org.vaadin.addon.calendar.ui.CalendarComponentEvents;

import java.time.Month;
import java.time.ZonedDateTime;
import java.util.GregorianCalendar;
import java.util.Random;

import static java.time.temporal.ChronoUnit.DAYS;

/**
 * Custom component that renders the Vaadin Calendar.
 *
 * SZ: Commented out setFullSize and any reference to sizing because it was
 *  messing up placement of Calendar.
 */
public class MeetingCalendar extends CustomComponent {

    private final Random R = new Random(0);
    private MeetingDataProvider eventProvider;
    private Calendar<MeetingItem> calendar;
    public Panel panel;

    public MeetingCalendar() {

        setId("meeting-meetings");
        //setSizeFull();

        initCalendar();

        VerticalLayout layout = new VerticalLayout();
        //layout.setMargin(false);
        //layout.setSpacing(false);
        //layout.setSizeFull();

        panel = new Panel(calendar);
        //panel.setHeight(100, Unit.PERCENTAGE);
        //panel.setSizeFull();
        layout.addComponent(panel);

        setCompositionRoot(layout);
    }

    public void switchToMonth(Month month) {
        calendar.withMonth(month);
    }

    public Calendar<MeetingItem> getCalendar() {
        return calendar;
    }

    /**
     * This seems to add a junk event directly to the calendar w/out user interaction.
     * @param event
     */
    private void onCalendarRangeSelect(CalendarComponentEvents.RangeSelectEvent event) {

        ZonedDateTime startDay = event.getStart().truncatedTo(DAYS);
        ZonedDateTime endDay = event.getEnd().truncatedTo(DAYS);

        boolean longTime = event.getStart().truncatedTo(DAYS).equals(event.getEnd().truncatedTo(DAYS));
        Meeting meeting = new Meeting(!longTime);

        meeting.setStart(event.getStart());
        meeting.setEnd(event.getEnd());

        meeting.setName("A Name");
        meeting.setDetails("A Detail<br> with HTML<br> with more lines");

        // Random state
        meeting.setState(R.nextInt(2)==1 ? Meeting.State.planned : Meeting.State.confirmed);

        eventProvider.addItem(new MeetingItem(meeting));
    }

    /**
     * This seems to add a junk event directly to the calendar w/out user interaction.
     * @param event
     */
    private void onCalendarClick(CalendarComponentEvents.ItemClickEvent event) {

        System.out.println("onCalendarClick");

        MeetingItem item = (MeetingItem) event.getCalendarItem();

        final Meeting meeting = item.getMeeting();

        Notification.show(meeting.getName(), meeting.getDetails(), Notification.Type.HUMANIZED_MESSAGE);
    }

    /**
     *
     */
    private void initCalendar() {

        eventProvider = new MeetingDataProvider();
        calendar = new Calendar<>(eventProvider);

        // This is the only sizing that is on, appears we have a winner.
        calendar.addStyleName("meetings");
        calendar.setWidth(100.0f, Unit.PERCENTAGE);
        calendar.setHeight(100.0f, Unit.PERCENTAGE);
        calendar.setResponsive(true);

        // Not sure if we want/need to do this.  Default behavior is false.
        calendar.setItemCaptionAsHtml(true);
        calendar.setContentMode(ContentMode.HTML);

        // These four lines were commented out in the example.
        //calendar.setLocale(Locale.US);
        //calendar.setZoneId(ZoneId.of("America/New York"));
        //calendar.setWeeklyCaptionProvider(date -> "<br>" + DateTimeFormatter.ofPattern("dd.MM.YYYY", getLocale()).format(date));
        //calendar.setWeeklyCaptionProvider(date -> DateTimeFormatter.ofLocalizedDate(FormatStyle.MEDIUM).withLocale(getLocale()).format(date));

        calendar.withVisibleDays(1, 7);
        //calendar.withMonth(ZonedDateTime.now().getMonth());

        // ORIG code
        //calendar.setStartDate(ZonedDateTime.of(2018, 9, 10, 0, 0, 0, 0, calendar.getZoneId()));
        //calendar.setEndDate(ZonedDateTime.of(2018, 9, 16, 0, 0, 0, 0, calendar.getZoneId()));

        // SZ code, set the default view to split the month
        calendar.setStartDate(ZonedDateTime.now().minusDays(15));
        calendar.setEndDate(ZonedDateTime.now().plusDays(15));

        addCalendarEventListeners();

        setupBlockedTimeSlots();
    }


    /**
     * Is this supposed to display something? If so, i do not see the items.
     */
    private void setupBlockedTimeSlots() {

        java.util.Calendar cal = java.util.Calendar.getInstance();

        cal.set(java.util.Calendar.HOUR_OF_DAY, 0);
        cal.clear(java.util.Calendar.MINUTE);
        cal.clear(java.util.Calendar.SECOND);
        cal.clear(java.util.Calendar.MILLISECOND);

        GregorianCalendar bcal = new GregorianCalendar(UI.getCurrent().getLocale());
        bcal.clear();

        long start = bcal.getTimeInMillis();

        // Adds 7 hours and 30 minutes
        bcal.add(java.util.Calendar.HOUR, 7);
        bcal.add(java.util.Calendar.MINUTE, 30);

        long end = bcal.getTimeInMillis();

        calendar.addTimeBlock(start, end, "my-blocky-style");

        cal.add(java.util.Calendar.DAY_OF_WEEK, 1);

        bcal.clear();
        bcal.add(java.util.Calendar.HOUR, 14);      // Should this even work? HOUR_OF_DAY is 24H
        bcal.add(java.util.Calendar.MINUTE, 30);
        start = bcal.getTimeInMillis();

        bcal.add(java.util.Calendar.MINUTE, 60);
        end = bcal.getTimeInMillis();

        calendar.addTimeBlock(start, end);
    }

    /**
     *
     */
    private void addCalendarEventListeners() {
        calendar.setHandler(new BasicDateClickHandler(true));
        calendar.setHandler(this::onCalendarClick);
        calendar.setHandler(this::onCalendarRangeSelect);
    }
    /**
     * Removes events, but why private final class?
     *
     * TODO: fill in this comment when you figure out why
     *
     */
    private final class MeetingDataProvider extends BasicItemProvider<MeetingItem> {

        void removeAllEvents() {
            this.itemList.clear();
            fireItemSetChanged();
        }
    }
}
