package com.aym.lucius.ui.views.calendar.meeting;

import java.time.ZonedDateTime;

/**
 * Basic object to represent an item on a calendar.
 * 'Borrowed' from demo
 */
public class Meeting {

    public enum State {
        empty,
        planned,
        confirmed
    }

    private ZonedDateTime start;
    private ZonedDateTime end;
    private String name;
    private String details;
    private State state = State.empty;
    private boolean longTime;

    public Meeting(boolean longTime) {
        this.longTime = longTime;
    }

    public ZonedDateTime getStart() {
        return start;
    }

    public void setStart(ZonedDateTime start) {
        this.start = start;
    }

    public ZonedDateTime getEnd() {
        return end;
    }

    public void setEnd(ZonedDateTime end) {
        this.end = end;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    public boolean isLongTime() {
        return longTime;
    }

    public void setLongTime(boolean longTime) {
        this.longTime = longTime;
    }

    public boolean isEditable() {
        return state != State.confirmed;
    }
}
