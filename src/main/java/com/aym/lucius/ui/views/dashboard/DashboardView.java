package com.aym.lucius.ui.views.dashboard;

import com.aym.lucius.ui.navigation.NavigationManager;
import com.vaadin.navigator.View;
import com.vaadin.spring.annotation.SpringView;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;


/**
 *  Landing page for clients to view various dashlets they configure
 */
@SpringView
public class DashboardView extends DashboardViewDesign implements View {

    private final NavigationManager navigationManager;

    @Autowired
    public DashboardView(NavigationManager navigationManager) {
        this.navigationManager = navigationManager;
    }

    @PostConstruct
    public void init() {

        setResponsive(true);
    }
}
