package com.aym.lucius.ui.views.employees;

import com.aym.lucius.backend.entity.Employee;
import com.aym.lucius.backend.service.EmployeeService;
import com.vaadin.data.Binder;
import com.vaadin.ui.*;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 */
public class EmployeeForm extends Window /*FormLayout*/ {

    // Data components
    private TextField code = new TextField("Code");
    private TextField ssn = new TextField("SSN");
    private TextField firstNameMI = new TextField("First Name");
    private TextField lastName = new TextField("Last Name");
    private TextField credentials = new TextField("Credentials");
    private TextField gender = new TextField("Gender");
    private TextField raceDescription = new TextField("Race");
    private TextField busEmpStatusDesc = new TextField("Status");

    // Date Fields
    private DateField dateOfBirth = new DateField("Date of Birth");
    private DateField dateEmployed = new DateField("Date Employed");
    private DateField dateTerminated = new DateField("Date Terminated");

    // Action components
    private Button btnSave = new Button("Save");
    private Button btnDelete = new Button("Delete");
    private Button btnCancel = new Button("Cancel");

    // Binder
    private Binder<Employee> binder = new Binder<>(Employee.class);

    // Associated objects
    @Autowired
    private EmployeeService employeeService;
    private Employee employee;
    private EmployeeView view;

    /**
     *
     * @param view
     */
    public EmployeeForm(EmployeeView view) {
        // Window specific
        super("Hope this works");

        this.view = view;

        // configures binder to use all similarly named editor fields
        //  in the form as on the Employee class.
        binder.bindInstanceFields(this);

        // Enable the buttons
        btnSave.addClickListener(e -> save());
        btnDelete.addClickListener(e -> delete());
        btnCancel.addClickListener(e -> cancel());

        // FormLayout
        //addComponents(code, ssn, firstNameMI, lastName, credentials,
        //        dateOfBirth, gender, dateEmployed, dateTerminated, btnCancel);

        // Window specific
        center();
        setClosable(false);

        VerticalLayout layout = new VerticalLayout();

        HorizontalLayout basicInfo = new HorizontalLayout();
        Label lblBasic = new Label("Basic Information");
        VerticalLayout basicColumn1 = new VerticalLayout(firstNameMI, lastName/*, ein*/);
        VerticalLayout basicColumn2 = new VerticalLayout(code, ssn/*, isInactive */);
        VerticalLayout basicColumn3 = new VerticalLayout(dateTerminated, busEmpStatusDesc/*, changePicture*/);
        basicInfo.addComponents(lblBasic, basicColumn1, basicColumn2, basicColumn3);

        HorizontalLayout otherInfo = new HorizontalLayout();
        Label lblOther = new Label("Other Information");
        VerticalLayout otherColumn1 = new VerticalLayout(dateEmployed, dateOfBirth/*, ein*/);
        VerticalLayout otherColumn2 = new VerticalLayout(gender/*, isInactive */);
        VerticalLayout otherColumn3 = new VerticalLayout(raceDescription, credentials/*, changePicture*/);
        otherInfo.addComponents(lblOther, otherColumn1, otherColumn2, otherColumn3);

        //form.addComponents(code, ssn,  credentials,
        //        dateOfBirth, gender, dateEmployed, dateTerminated,
        //        raceDescription, busEmpStatusDesc);
        layout.addComponents(basicInfo);
        layout.addComponents(otherInfo);
        layout.addComponents(btnCancel);

        layout.setSizeFull();
        layout.setSpacing(true);
        layout.setMargin(true);
        layout.setSizeUndefined();

        setContent(layout);

        setEmployee(null);
    }

    /**
     *
     * @param employee
     */
    public void setEmployee(Employee employee) {

        this.employee = employee;

        // Initializes all fields in the form and updates values
        //  in the object as corresponding field values change in UI.
        binder.setBean(employee);

        boolean enabled = employee != null;
        btnSave.setEnabled(enabled);
        btnDelete.setEnabled(enabled);

        if (enabled)
            firstNameMI.focus();
    }

    /**
     * TODO: move to interface, abstract classes
     */
    private void save() {

    }

    /**
     * TODO: move to interface, abstract classes
     */
    private void delete() {

    }

    /**
     * TODO: move to interface, abstract classes
     */
    private void cancel() {
        this.close();
    }

}
