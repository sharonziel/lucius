package com.aym.lucius.ui.views.login;

import com.vaadin.annotations.DesignRoot;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.declarative.Design;

@DesignRoot
public class AccessDeniedDesign extends VerticalLayout {

    public AccessDeniedDesign()  {
        Design.read(this);
    }
}
