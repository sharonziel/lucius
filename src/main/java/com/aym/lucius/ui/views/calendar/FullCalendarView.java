package com.aym.lucius.ui.views.calendar;

import com.aym.lucius.ui.navigation.NavigationManager;
import com.vaadin.navigator.View;
import com.vaadin.server.Page;
import com.vaadin.spring.annotation.SpringView;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;

@SpringView
public class FullCalendarView extends FullCalendarViewDesign implements View {

    private final NavigationManager navigationManager;

    @Autowired
    public FullCalendarView(NavigationManager navigationManager) {
        this.navigationManager = navigationManager;
    }

    @PostConstruct
    public void init() {

        setResponsive(true);

        String js = "alert('Hello from the other side')";

        String jsInitFullCalendar = "" +
                "    $(function() {\n" +
                "        $('#calendar').fullCalendar({\n" +
                "            header: {\n" +
                "                left: 'prev, next today',\n" +
                "                center: 'title',\n" +
                "                right: 'month,agendaweek,agendaDay,listWeek'\n" +
                "            },\n" +
                "            defaultDate: '2018-09-27',\n" +
                "            navLinks: true,\n" +
                "            editable: true,\n" +
                "            eventLimit: true,\n" +
                "            events: [\n" +
                "                {\n" +
                "                    title: 'All Day Event',\n" +
                "                    start: '2018-09-20'\n" +
                "                },\n" +
                "                {\n" +
                "                    title: 'Meeting',\n" +
                "                    start: '2018-09-12T08:30:00',\n" +
                "                    end: '2018-09-12T10:30:00'\n" +
                "                }\n" +
                "            ]\n" +
                "        })\n" +
                "    });\n" +
                "";
        Page.getCurrent().getJavaScript().execute(js);
        Page.getCurrent().getJavaScript().execute(jsInitFullCalendar);
        Page.getCurrent().getJavaScript().execute("alert('after init')");

    }

}
