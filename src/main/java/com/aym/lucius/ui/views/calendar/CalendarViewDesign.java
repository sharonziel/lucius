package com.aym.lucius.ui.views.calendar;

import com.vaadin.annotations.DesignRoot;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.declarative.Design;

@DesignRoot
public class CalendarViewDesign extends VerticalLayout {

    public CalendarViewDesign() {
        Design.read(this);
    }
}
