package com.aym.lucius.ui.views.employees;

import com.vaadin.annotations.DesignRoot;
import com.vaadin.ui.Grid;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.declarative.Design;

@DesignRoot
public class EmployeeViewDesign extends VerticalLayout {

    //protected Grid employeeList;

    public EmployeeViewDesign()  {
        Design.read(this);
    }
}
