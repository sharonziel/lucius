package com.aym.lucius.ui.views.dashboard;

import com.vaadin.annotations.DesignRoot;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.declarative.Design;

@DesignRoot
public class DashboardViewDesign extends VerticalLayout {

    public DashboardViewDesign() { Design.read(this); }
}
