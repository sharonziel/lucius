package com.aym.lucius.ui;

import com.vaadin.server.*;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.ui.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.session.SessionAuthenticationStrategy;

import java.net.MalformedURLException;
import java.net.URI;

@SuppressWarnings("serial")
@SpringUI(path="/login")
public class LoginUI extends UI {

    @Autowired private AuthenticationProvider authenticationProvider;

    @Autowired
    SessionAuthenticationStrategy sessionAuthenticationStrategy;

    @Override
    protected void init(final VaadinRequest request) {

        if (!(SecurityContextHolder.getContext().getAuthentication() instanceof AnonymousAuthenticationToken)) {
            URI currentLoc = Page.getCurrent().getLocation();
            try {
                Page.getCurrent().setLocation(currentLoc.toURL().toString().replace("/login", "/app"));

            } catch (MalformedURLException malEx) {
                malEx.printStackTrace();
            }

            return;
        }

        VerticalLayout layout = new VerticalLayout();
        LoginForm loginForm = new LoginForm();
        loginForm.addLoginListener(e -> {
            String username = e.getLoginParameter("username");
            String password = e.getLoginParameter("password");
            final Authentication auth = new UsernamePasswordAuthenticationToken(username, password);
            try {
                // this is the code for achieving the spring security authentication in a programmatic way
                final Authentication authenticated = authenticationProvider.authenticate(auth);
                SecurityContextHolder.getContext().setAuthentication(authenticated);
                sessionAuthenticationStrategy.onAuthentication(auth,
                        ((VaadinServletRequest)VaadinService.getCurrentRequest()).getHttpServletRequest(),
                        ((VaadinServletResponse)VaadinService.getCurrentResponse()).getHttpServletResponse()
                );
                URI currentLoc = Page.getCurrent().getLocation();
                try {
                    Page.getCurrent().setLocation(currentLoc.toURL().toString().replace("/login", "/app"));
                } catch (MalformedURLException malEx) {
                    malEx.printStackTrace();
                }

            } catch (final AuthenticationException aEx) {
                String message = "Incorrect username or password:" + aEx.getMessage() +
                        e.getLoginParameter("username") + ":" +
                        e.getLoginParameter("password");
                Notification.show(message, Notification.Type.ERROR_MESSAGE);
            }
        });

        layout.addComponent(loginForm);
        layout.setComponentAlignment(loginForm, Alignment.MIDDLE_CENTER);
        layout.setSizeFull();
        setContent(layout);
    }
}
