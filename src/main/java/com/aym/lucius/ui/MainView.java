package com.aym.lucius.ui;

import com.aym.lucius.ui.views.calendar.CalendarView;
import com.aym.lucius.ui.views.calendar.FullCalendarView;
import com.aym.lucius.ui.views.dashboard.DashboardView;
import com.aym.lucius.ui.views.employees.EmployeeView;
import com.aym.lucius.ui.navigation.NavigationManager;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewDisplay;
import com.vaadin.navigator.ViewLeaveAction;
import com.vaadin.spring.access.SecuredViewAccessControl;
import com.vaadin.spring.annotation.SpringViewDisplay;
import com.vaadin.spring.annotation.UIScope;
import com.vaadin.ui.Button;
import com.vaadin.ui.UI;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.Map;

/**
 * This is the main view containing the navigation and content
 *  area where individual views are shown.
 *  'Borrowed' from V8 Starter.
 */
@SpringViewDisplay
@UIScope
public class MainView extends MainViewDesign implements ViewDisplay {

    private final Map<Class<? extends View>, Button> navigationButtons = new HashMap<>();
    private final NavigationManager navigationManager;
    private final SecuredViewAccessControl viewAccessControl;

    /**
     * Constructor to autowire our navigation and permissions controllers
     * Commenting out security for now, will add back in later
     * @param navManager
     */
    @Autowired
    public MainView(NavigationManager navManager, SecuredViewAccessControl viewAccessControl) {
        this.navigationManager = navManager;
        this.viewAccessControl = viewAccessControl;
    }

    /**
     * @PostConstruct - Delays execution of this method until after DI is done.  Method MUST
     *  be invoked before class is put into service
     */
    @PostConstruct
    public void init() {
        attachNavigation(dashboard, DashboardView.class);
        attachNavigation(employees, EmployeeView.class);
        attachNavigation(vCalendar, CalendarView.class);
        attachNavigation(fullCalendar, FullCalendarView.class);
    }

    /**
     * Clears out current view and replaces it with the one selected
     * @param view
     */
    @Override
    public void showView(View view) {

        content.removeAllComponents();
        content.addComponent(view.getViewComponent());

        navigationButtons.forEach((viewClass, button) -> button.setStyleName("selected", viewClass == view.getClass()));

        Button menuItem = navigationButtons.get(view.getClass());
        String viewName = "";
        if (menuItem != null) {
            viewName = menuItem.getCaption();
        }
        activeViewName.setValue(viewName);
    }

    /**
     *
     * @param navigationButton
     * @param targetView
     */
    private void attachNavigation(Button navigationButton, Class<? extends View> targetView) {

        boolean hasAccess = true; // later we can check credentials

        if (hasAccess) {
            navigationButtons.put(targetView, navigationButton);
            navigationButton.addClickListener(e -> navigationManager.navigateTo(targetView));

        }
    }

    /**
     *
     */
    public void logout() {
        ViewLeaveAction doLogout = () -> {
            UI ui = getUI();
            ui.getSession().getSession().invalidate();
            ui.getPage().reload();
        };

        // triggers View.beforeLeave() for the current view.
        navigationManager.runAfterLeaveConfirmation(doLogout);
    }
}
