package com.aym.lucius.ui.navigation;

import com.aym.lucius.ui.views.dashboard.DashboardView;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.spring.annotation.UIScope;
import com.vaadin.spring.internal.Conventions;
import com.vaadin.spring.navigator.SpringNavigator;
import org.springframework.stereotype.Component;

/**
 * Controls the view navigation
 * 'Borrowed' from V8 Starter
 */
@Component
@UIScope
public class NavigationManager extends SpringNavigator {

    /**
     *
     * @param viewClass
     * @return
     */
    public String getViewId(Class<? extends View> viewClass) {
        SpringView springView = viewClass.getAnnotation(SpringView.class);
        if (springView == null) {
            throw new IllegalArgumentException("The target class must be @SpringView");
        }

        return Conventions.deriveMappingForView(viewClass, springView);
    }

    /**
     *
     * @param targetView
     */
    public void navigateTo(Class<? extends View> targetView) {
        String viewId = getViewId(targetView);
        navigateTo(viewId);
    }

    public void navigateTo(Class<? extends View> targetView, Object parameter) {
        String viewId = getViewId(targetView);
        navigateTo(viewId + "/" + parameter.toString());
    }

    /**
     * If the user wants a specfic view, its in the URL.  Otherwise,
     *  we can set specific navigation based on permissions (later)
     */
    public void navigateToDefaultView() {
        if (!getState().isEmpty()) {
            return;
        }
        navigateTo(DashboardView.class);
    }

    public void updateViewParameter(String parameter) {
        String viewName = getViewId(getCurrentView().getClass());
        String parameters;
        if (parameter == null) {
            parameters = "";
        } else {
            parameters = parameter;
        }

        updateNavigationState(new ViewChangeEvent(this, getCurrentView(), getCurrentView(), viewName, parameters));
    }

}
