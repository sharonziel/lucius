package com.aym.lucius.ui.util;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * https://vaadin.com/forum/thread/9708998
 */
public class MouseClickManager {

    public static long CLICK_TIME = 250;

    private static Map<Object, Integer> clickcountMap = new HashMap<Object, Integer>();

    private static Map<Object, Long> lastClickMap = new HashMap<Object, Long>();

    public static int handleClick( Object source ) {

        int count = 1;

        if(source == null) {
            return count;
        }

        long now = System.currentTimeMillis();
        Long lastClick = lastClickMap.get(source);



        if(lastClick == null) {
            lastClickMap.put(source, now);
            clickcountMap.put(source, count);
        } else if((now - lastClick) < CLICK_TIME ) {
            lastClickMap.put(source, now);
            count = clickcountMap.get(source);
            count++;
            clickcountMap.put(source, count);
        } else {
            lastClickMap.put(source, now);
            clickcountMap.put(source, count);
        }

        return count;
    }

}
