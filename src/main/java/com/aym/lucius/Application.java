package com.aym.lucius;

import com.aym.lucius.app.security.SecurityConfig;
import com.aym.lucius.ui.AppUI;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.vaadin.spring.events.annotation.EnableEventBus;

/**
 *
 */
@SpringBootApplication(scanBasePackageClasses = { AppUI.class, Application.class, SecurityConfig.class })
@EnableEventBus
public class Application extends SpringBootServletInitializer {

    public static final String APP_URL = "/";
    public static final String LOGIN_URL = "/login/LoginViewDesign.html";
    public static final String LOGOUT_URL = "/";  //"/login.html?logout";
    public static final String LOGIN_FAILURE_URL = "/login/AccessDeniedDesign.html";
    public static final String LOGIN_PROCESSING_URL = "/login";

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(Application.class);
    }

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }


}
