9/20/2018 - At a logical stopping point, cutting over to work on OTB issue.  As it stands now, application has the following:
 - basic navigation functionality
 - loading an Employee Json file into grid
 - double click grid to open modal window
 - Grid issue with sizing height to available viewport, have asked Jason Stiefel to help.
 - Calendars
 	- Vaadin calendar is rendering, but not displaying any meaningful data. Unsupported add-on, very little documentation found here:
		-https://vaadin.com/directory/component/calendar-add-on/2.0-BETA4
		AND
		-https://www.programcreek.com/java-api-examples/?code=blackbluegl/calendar-component/calendar-component-master/calendar-component-addon/src/main/java/org/vaadin/addon/calendar/Calendar.java#
	- FullCalendar.io is working in html page (localhost:8080/FullCalendarTest.html) but have not figured out how to get it to render in Vaadin.
		- Needs to be installed (css and js files not included in repo) see https://fullcalendar.io/ for docs.
		- Currently undergoing major revision to remove Jquery. In alpha as of this writing.
		
	